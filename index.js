const request = require('request-promise');
const cheerio = require('cheerio');
const fs = require('fs');
const Json2csvParser = require('json2csv').Parser;

const URLS = [
  "https://www.imdb.com/title/tt0102926/",
  "https://www.imdb.com/title/tt0114369/?ref_=tt_rec_tti"];

(async () => {
  let moviesData = [];
  for (let movies of URLS) {
    const response = await request({
      url: movies,
      headers: {
        Accept:
          "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "en,lt;q=0.8,en-US;q=0.6,ru;q=0.4,pl;q=0.2",
        "Accept-Encoding": "gzip, deflate",
        "Connection": "keep-alive",
        "Host": "www.imdb.com",
        "Upgrade-Insecure-Requests": "1",
        "Cache-Control": "max-age=0",
        "User-Agent":
          "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0"
      },
      gzip: true,

    }
    );
    let $ = cheerio.load(response);
    let title = $('div[class="title_wrapper"] > h1 ').text().trim();
    let raiting = $('span[itemprop="ratingValue"]').text();
    let poster = $('div[class="poster"] > a > img').attr('src');
    let totaly_raiting = $('span[class="small"]').text();
    let relese_data = $('a[title="See more release dates"]').text().trim();



    let genres = [];
    $('div[class="title_wrapper"]  a[href^="/search/title?genres"]').each((i, elem) => {
      let genre = $(elem).text();
      genres.push(genre);
    });

    moviesData.push({
      movies,
      title,
      raiting,
      poster,
      totaly_raiting,
      relese_data
    });


  }
  const fields = ['title', 'raiting'];
  const json2csvParser = new Json2csvParser({ fields });
  const csv = json2csvParser.parse(moviesData);
  console.log(csv);
  // fs.writeFileSync('./data.json', JSON.stringify(moviesData), 'utf-8');

})();
